<?php
namespace App\Entity;

class BrowserCategory {
    const File = 1;
    const Document = 2;
    const Image = 3;
    const Attachment = 4;
}